import xlsxwriter


origen1="GPS160704A.txt"
origen2="DT5203160704-MOVIL.txt"
origen3="PT0013160704-MOVIL.txt"
destino="capa_04_07_2016_correccion.xlsx"

O1=open(origen1,"r")
O2=open(origen2,"r")
O3=open(origen3,"r")
Latitud=[]
Longitud=[]
Date=[]
Hora=[]
Altitude=[]
Depth=[]
Speed=[]
Heading=[]
Dust_Track=[]
P_Track=[]

#Datos GPS
for columnas in O1:
    columnas=columnas.strip()
    columnas=columnas.split()

    try:
        if(columnas[-1]=="true"):
            var=columnas[1]
            var=var[1:]
            var=float(var)
            var2=float(columnas[2])
            var2=var2/60
            var=var+var2
            Latitud.append(var*-1)
            var=columnas[3]
            var=var[1:]
            var=float(var)
            var2=float(columnas[4])
            var2=var2/60
            var=var+var2
            Longitud.append(var*-1)
            Date.append(columnas[5])
            var=columnas[6]
            if(len(var)<8):
                Hora.append('0'+var)
            else:
                Hora.append(var)
            Altitude.append(columnas[7])
            Depth.append(columnas[9])
            if columnas[12] != "true":

                Speed.append(columnas[12])
            var=columnas[14]
            var=var[0:-1]
            Heading.append(var)

    except:

        continue


#Datos Dust track
DT={}
for columnas in O2:
    try:
        columnas=columnas.split("")

    except:
        columnas=columnas.split()
    try:
        DT[columnas[1]]=columnas[2]
    except:
        continue

#Datos P track
PT={}
for columnas in O3:
    columnas=columnas.split()
    try:
        PT[columnas[1]]=columnas[2]
    except:
        continue

#Coincidir y grabar
for i in range(len(Hora)):
    try:
        Dust_Track.append((DT[Hora[i]]))

        P_Track.append(float(PT[Hora[i]]))
    except:
        continue

workbook = xlsxwriter.Workbook(destino)
worksheet = workbook.add_worksheet()
worksheet.write('A1', 'Latitud')
worksheet.write('B1', 'Longitud')
worksheet.write('C1', 'Fecha')
worksheet.write('D1', 'Hora')
worksheet.write('E1', 'Altitud')
worksheet.write('F1', 'Profundidad')
worksheet.write('G1', 'Velocidad')
worksheet.write('H1', 'Referencia')
worksheet.write('I1', 'Dust-Track')
worksheet.write('J1', 'P-Track')

for i in range(2,len(P_Track)):
    worksheet.write('A'+str(i),Latitud[i])
    worksheet.write('B'+str(i),Longitud[i])
    worksheet.write('C'+str(i),Date[i])
    worksheet.write('D'+str(i),Hora[i])
    worksheet.write('E'+str(i),float(Altitude[i]))
    worksheet.write('F'+str(i),float(Depth[i]))
    worksheet.write('G'+str(i),float(Speed[i]))
    worksheet.write('H'+str(i),float(Heading[i]))
    try:
        worksheet.write('I'+str(i),float(Dust_Track[i]))
    except:
        worksheet.write('I'+str(i),float(Dust_Track[i]))

    worksheet.write('J'+str(i),(P_Track[i]))

workbook.close()

print "Archivo grabado con exito..."


O1.close()
O2.close()
O3.close()


